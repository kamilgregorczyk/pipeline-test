package com.kgregorczyk;

import static java.net.HttpURLConnection.HTTP_OK;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Random;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.junit.jupiter.api.Test;

public class MainServerTest {

  @Test
  public void testIndex() throws Exception {
    // given
    var port = new Random().nextInt(1000) + 8000;
    var args = new String[1];
    args[0] = String.valueOf(port);
    MainServer.main(args);
    var client = HttpClients.createDefault();
    var request = new HttpGet("http://localhost:" + port);

    // when
    CloseableHttpResponse response = client.execute(request);

    // then
    assertEquals(response.getStatusLine().getStatusCode(), HTTP_OK);
  }
}
