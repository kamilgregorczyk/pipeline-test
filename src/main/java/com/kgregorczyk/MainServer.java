package com.kgregorczyk;

import static spark.Spark.get;
import static spark.Spark.port;

public class MainServer {
  public static void main(String[] args) {
    if (args.length >= 1) {
      port(Integer.valueOf(args[0]));
    } else {
      port(8000);
    }
    get("/", (request, response) -> "OK 2!");
    get("/hello", (request, response) -> request.params("name"));
  }
}
